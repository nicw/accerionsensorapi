/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#ifndef SERIALIZATION_H_
#define SERIALIZATION_H_

#include <cstdint>
#include "TypeDef.h"

namespace Serialization
{
    /**
     * \brief Method to serialize Uint16 to pointer of Uint8
     * \param in uint16_t the 16 bit value that has to be serialized
     * \param out uint8_t pointer where the values should be stored
     * \param bigEndian bool to toggle the endianness
     **/
    void serializeUInt16(uint16_t in, uint8_t* out, bool bigEndian);

    /**
     * \brief Method to serialize Uint32 to pointer of Uint8
     * \param in uint32_t the 32 bit value that has to be serialized
     * \param out uint8_t pointer where the values should be stored
     * \param bigEndian bool to toggle the endianness
     **/
    void serializeUInt32(uint32_t in, uint8_t* out, bool bigEndian);

    /**
     * \brief Method to serialize Uint64 to pointer of Uint8
     * \param in uint64_t the 64 bit value that has to be serialized
     * \param out uint8_t pointer where the values should be stored
     * \param bigEndian bool to toggle the endianness
     **/
    void serializeUInt64(uint64_t in, uint8_t* out, bool bigEndian);

    /**
     * \brief Method to convert from network to host order
     * \param input uint64_t the 64 bit value that has to be converted
     * \return uint64_t in host order
     **/
    uint64_t ntoh64(const uint64_t *input);

    /**
     * \brief Method to convert from host to network order
     * \param input uint64_t the 64 bit value that has to be converted
     * \return uint64_t in network order
     **/
    uint64_t hton64(const uint64_t *input);
}

#endif