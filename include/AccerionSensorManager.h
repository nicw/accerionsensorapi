/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include <list>
#include <string>
#include <iostream>
#include <vector>
#include <thread>
#include <sstream>
#include "TypeDef.h"
#include "commands.h"
#include "CRC8.h"
#include "AccerionSensor.h"
#include "UDPReceiver.h"
#include "UDPTransmitter.h"

#ifdef __linux__ 
    #include <arpa/inet.h>
#elif _WIN32
    #include <winsock2.h>
    #include <ws2tcpip.h>
    #include <stdio.h>

    #pragma comment(lib, "Ws2_32.lib")
#endif

/** Callback type definition that is used for the sensor objects*/
typedef std::function<void(AccerionSensor* ac)>     _sensorCallBack;

/// AccerionSensorManager class is responsible for detecting sensors in the local subnet and providing access to individual sensor objects
class AccerionSensorManager
{

    public:
        /* Static access method. */
        static AccerionSensorManager* getInstance();

        /**
         * \brief method to list all sensors
         * \return a list that contains pairs of IP addresses and serial numbers
         * 
         * This class listens to broadcast messages on a certain port. It tries to identify
         * different sensors and adds them to a list. This method returns that list and gives 
         * you access to the IP addresses and serial numbers of all the found sensors.
         **/
        std::list<std::pair<Address, std::string>> getAllSensors();

        /**
         * \brief method to get AccerionSensor object based on its IP address
         * \param sensorIP struct that contains the IP address of the sensor you'd like to connect to
         * \param localIP struct containing the local IP address, use an address the sensor can access
         * \param conType to provide the preferred connection method
         * \return AccerionSensor object which allows you to interact with that sensor.
         * 
         **/
        AccerionSensor* getAccerionSensorByIP(Address sensorIP, Address localIP, ConnectionType conType);


        /**
         * \brief method to get AccerionSensor object based on its IP address
         * \param sensorIP struct that contains the IP address of the sensor you'd like to connect to
         * \param localIP struct containing the local IP address, use an address the sensor can access
         * \param conType to provide the preferred connection method
         * \param timeoutValueInSeconds int value containing the time in seconds after the request should timeout
         * \return AccerionSensor object which allows you to interact with that sensor. Nullptr in case of timeout.
         **/
        AccerionSensor* getAccerionSensorByIPBlocking(Address sensorIP, Address localIP, ConnectionType conType, int timeoutValueInSeconds);


        /**
         * \brief method to get AccerionSensor object based on its IP address
         * \param sensorIP struct that contains the IP address of the sensor you'd like to connect to
         * \param localIP struct containing the local IP address, use an address the sensor can access
         * \param conType to provide the preferred connection method
         * \param scallback _sensorCallBack that is invoked when sensor is found
         * \param timeoutValueInSeconds int value containing the time in seconds after the request should timeout
         * 
         **/
        void getAccerionSensorByIP(Address sensorIP, Address localIP, ConnectionType conType, _sensorCallBack scallback);

        /**
         * \brief method to get AccerionSensor object based on its serial number
         * \param serial contains the serial of the sensor you'd like to connect to
         * \param localIP struct containing the local IP address, use an address the sensor can access
         * \param conType to provide the preferred connection method
         * \return AccerionSensor object which allows you to interact with that sensor.
         * 
         **/
        AccerionSensor* getAccerionSensorBySerial(std::string serial, Address localIP, ConnectionType conType);

    private:
        /**
         * \brief  Private constructor to prevent instancing. 
         **/
        AccerionSensorManager();
        /** 
         * \brief Instantation prevention 
         */ 
        AccerionSensorManager(const AccerionSensorManager&) = delete;
        /** 
         * \brief Instantation prevention 
         */ 
        AccerionSensorManager& operator=(const AccerionSensorManager&) = delete;

        CRC8 crc8_;/*!< Used for message verification */
        bool debugMode_ = false;/*!< Flag used for debugging purposes */

        std::list<std::pair<Address, std::string>> sensors;/*!< list of pairs that contain the ip address and serial number for each detected sensor */
        std::vector<uint8_t>    receivedCommand_;/*!< Holds incoming UDP command (size depends on UDP command ID) */
        uint8_t                 receivedCommandID_;/*!< Holds the commandID of the received command */
        bool                    lastMessageWasBroken_;/*!< Keeps track whether the last message was split up/broken */
        uint32_t                sensorSerialNumber_ = DEFAULT_SERIAL_NUMBER;/*!< Holds the unique serial number of the Jupiter unit */
        uint32_t                receivedSerialNumber_;/*!< Holds received serial number from incoming UDP message, used to determine if message is intended for this unit */
        uint8_t                 receivedCRC8_;/*!< Holds incoming CRC8 code (in 0xD8) , compared with CRC code computed at Jupiter side for error-checking */
        bool                    messageReady_;/*!< True if an incoming UDP message is ready, False otherwise. */

        /// Method that runs on separate thread and receives messages
        void runUDPCommunication();

        /**
         * @brief      parseMessages reads incomming network packages and dissects them. If a command is completely received it is put in the commands vector supplied in the param
         *
         * @param[in]  commands         vector of commands
         * @param[in]  receivedMessage_ vector of data
         */
        void parseMessage(std::vector<Command> &commands, std::vector<uint8_t> receivedMessage_);

        // SENSOR REQUEST //
        _sensorCallBack sensorCallBack = nullptr;
        std::mutex sensorRequestAckMutex;
        std::condition_variable sensorRequestAckCV;
        bool newSensorReceived = false;
        Address sensorIP_;
        Address localIP_;
        ConnectionType conType_;
        // END OF SENSOR REQUEST //

};


