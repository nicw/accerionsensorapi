/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#ifndef CRC8_H
#define CRC8_H
#include <cstdint>
#include "TypeDef.h"

typedef uint8_t crc;  //!< making uint8_t addressable as crc
#define WIDTH  (8 * sizeof(crc))  //!< definining the width of the CRC
#define POLYNOMIAL 0xD8   //!<  11011 followed by 0's
#define TOPBIT (1 << (WIDTH - 1))  //!< defining the top bit based on the width
extern crc crcTable[256];  //!< ...

/// CRC8 class that is intended for message verification
class CRC8
{
public:
	CRC8(); //!< Constructor for CRC8
	~CRC8();  //!< Destructor for CRC8
	void crcInit();  //!< Init method for the CRC8, this is required!

	/**
     * @brief     Method to generate a CRC8 based on input
     *
     * @param[in] message[] : array of uint8_t which is the data to be send
	 * @param[in] nBytes : total size of the provided data
	 * @return crc : returns the calculated crc8 over the inputted data
     */
	crc crcFast(uint8_t const message[], int nBytes);
};

#endif