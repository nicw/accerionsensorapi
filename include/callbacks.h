/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#ifndef CALLBACKS_H
#define CALLBACKS_H
#include <functional>
#include "TypeDef.h"
#include "structs.h"
#include "commands.h"

// STREAMING MSGS

/** Callback type definition that is used for the heartbeat message and has a HeartBeat struct as a parameter */
typedef std::function<void(HeartBeat hb)>           _heartBeatCallBack;

/** Callback type definition that is used for CorrectedPose output and has a CorrectedPose struct as a parameter */
typedef std::function<void(CorrectedPose cp)>       _correctedPoseCallBack;

/** Callback type definition that is used for UncorrectedPose output and has an UncorrectedPose struct as a parameter */
typedef std::function<void(UncorrectedPose cp)>     _uncorrectedPoseCallBack;

/** Callback type definition that is used for diagnostic messages and has a Diagnostics struct as a parameter */
typedef std::function<void(Diagnostics diag)>       _diagnosticsCallBack; 

/** Callback type definition that is used for Drift Corrections and has a DriftCorrection struct as a parameter */
typedef std::function<void(DriftCorrection dc)>     _driftCorrectionCallBack; 

/** Callback type definition that is used for Quality Estimates and has a QualityEstimate struct as a parameter */
typedef std::function<void(QualityEstimate qe)>     _qualityEstimateCallBack; 

/** Callback type definition that is used for LineFollower Data and has a LineFollowerData struct as a parameter */
typedef std::function<void(LineFollowerData lf)>    _lineFollowerCallBack; 

/** Callback type definition that is used for Marker Messages and has a MarkerPosPacket as a parameter */
typedef std::function<void(MarkerPosPacket mp)>     _markerPosPacketCallBack; 

/** Callback type definition that is used for Console Output Messages and has a string as a parameter */
typedef std::function<void(std::string str)>        _consoleOutputCallback; 


// CMD - ACK MSGS
/** Callback type definition that is used for various methods that have an Acknowledgement struct as a parameter */
typedef std::function<void(Acknowledgement ack)>    _acknowledgementCallBack; 

/** Callback type definition that is used for getting and setting the IP address and has an IPAddressExtended struct as a parameter */
typedef std::function<void(IPAddressExtended ip)>   _ipAddressCallBack; 

/** Callback type definition that is used for getting and setting the Sample Rate and has a SampleRate struct as a parameter */
typedef std::function<void(SampleRate sr)>          _sampleRateCallBack; 

/** Callback type definition that is used for getting the Serial Number and has a SerialNumber struct as a parameter */
typedef std::function<void(SerialNumber sr)>        _serialNumberCallBack; 

/** Callback type definition that is used for getting the Software Version and has a SoftwareVersion struct as a parameter */
typedef std::function<void(SoftwareVersion sv)>     _softwareVersionCallBack; 

/** Callback type definition that is used for getting the TCP/IP information and has a TCPIPInformation struct as a parameter */
typedef std::function<void(TCPIPInformation ip)>    _tcpIPInformationCallBack; 

/** Callback type definition that is used for removing QR's and has a uint16_t as a parameter indicating the removed QR ID if successfull*/
typedef std::function<void(uint16_t qrID)>          _removeQRCallBack; 

/** Callback type definition that is used for removing a Cluster and has a uint16_t as a parameter indicating the removed Cluster ID if succesfull*/
typedef std::function<void(uint16_t qrID)>          _removeClusterCallBack; 

/** Callback type definition that is used for secondary line follower data requests and has a LineFollowerData struct as a parameter */
typedef std::function<void(LineFollowerData lf)>    _secondaryLineFollowerCallBack; 

/** Callback type definition that is used for adding QR markers and has an AddQRResult as a parameter */
typedef std::function<void(AddQRResult ar)>         _addQRCallBack; 

/** Callback type definition that is used for setting the Date/Time and has a DateTime struct as a parameter */
typedef std::function<void(DateTime dt)>            _dateTimeCallBack; 

/** Callback type definition that is used for setting the pose and has a Pose struct as a parameter */
typedef std::function<void(Pose sp)>                _poseCallBack; 

/** Callback type definition that is used for setting UDP Settings and has a UDPInfo struct as a parameter */
typedef std::function<void(UDPInfo udpInfo)>        _setUDPSettingsCallBack; 

/** Callback type definition that is used for getting a Cluster in G2O format and has a vector of uint8_t as a parameter, containing the data*/
typedef std::function<void(std::vector<uint8_t> data)> _clusterInG2OFormatCallBack; 

/** Callback type definition that is used for getting the Software Details and has a SoftwareDetails struct as a parameter */
typedef std::function<void(SoftwareDetails sd)>     _softwareDetailsCallBack;

/** Callback type definition that is used for Missed Drift Correction(s) messages and has an int as a parameter, indicating the amount of missed corrections */
typedef std::function<void(int dcm)>                _driftCorrectionsMissedCallBack; 

/** Callback type definition that is used for Detected Aruco Marker Messages and has an ArucoMarker struct as a parameter */
typedef std::function<void(ArucoMarker am)>         _arucoMarkerCallBack; 

/** Callback type definition that is used for Map Loaded Messages and has an MapLoadingInfo struct as a parameter */
typedef std::function<void(MapLoadingInfo mli)>         _mapLoadedCallBack;

/** Callback type definition that is used for getting and setting the buffer length of the Buffered Recovery Mode and has an int as a parameter, indicating the length in meter */
typedef std::function<void(int buffLength)>         _bufferLengthCallBack; 

/** Callback type definition that is used for progress messages of the Buffered Recovery Mode and has a BufferProgress struct as a parameter */
typedef std::function<void(BufferProgress bp)>      _bufferProgressCallBack; 

/** Callback type definition that is used for storing a list of recording names which has a vector of strings as a parameter */
typedef std::function<void(std::vector<std::string> recList)> _recordingListCallBack;

/** Callback type definition that is used for recording deletion and has a DeleteRecordingsResult as a parameter */
typedef std::function<void(DeleteRecordingsResult result)> _deleteRecordingsCallBack;


//FILE TRANSFER MSGS
/** Callback type definition that is used for file transfers and has an int as a parameter, indicating the progress in percentages(0-100) */
typedef std::function<void(int progress)>               _progressCallBack; 

/** Callback type definition that is used for file transfers and has a bool as a parameter, indicating a success or failure */
typedef std::function<void(bool done)>                  _doneCallBack; 

/** Callback type definition that is used for file transfers and has a FileSenderStatus enum as a parameter */
typedef std::function<void(FileSenderStatus status)>    _statusCallBack; 

#endif