/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "UDPTransmitter.h"

using namespace std;

UDPTransmitter::UDPTransmitter(unsigned int remoteReceivePort)
{
#ifdef DEBUG
    debugMode_ = true;
#endif

    crc8_.crcInit();

    remoteReceivePort_   = remoteReceivePort;

    socketEndpoint_ = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if(socketEndpoint_ < 0)
        cout << "Error while opening transmitting socket" << endl;
}

void UDPTransmitter::setIPAddress(struct in_addr ipAddress)
{
    /*Set the IP addresses for UDP message*/
    remoteAddress_.sin_family = AF_INET;
    remoteAddress_.sin_port = htons(remoteReceivePort_);
    remoteAddress_.sin_addr.s_addr = ipAddress.s_addr;
    memset(remoteAddress_.sin_zero, '\0', sizeof(remoteAddress_.sin_zero));

    if (debugMode_)
    {
        cout << "From UDP Transmitter, setting ip address to := " << inet_ntoa(remoteAddress_.sin_addr) << endl;
    }
}


bool UDPTransmitter::transmitMessage(uint8_t* transmittedMessage, unsigned int transmittedNumOfBytes)
{
    if (transmittedNumOfBytes > bufferSize_)
    {
        if(debugMode_)
            cout << "[UDP]Number of bytes is larger than maximum message size, number of bytes := " << transmittedNumOfBytes  << endl;
        return false;
    }
    if(sendto(socketEndpoint_, reinterpret_cast<char*>(&transmittedMessage[0]), transmittedNumOfBytes, 0, (struct sockaddr*) &remoteAddress_, sizeof(remoteAddress_)) == -1)
    {
        if(debugMode_)
             perror(" Error multicasting message to port, error is");
        return false;
    }
    return true;
}

UDPTransmitter::~UDPTransmitter()
{
#ifdef __linux__ 
    close(socketEndpoint_);
#elif _WIN32
    closesocket(socketEndpoint_);
#endif
}

uint32_t UDPTransmitter::formMessage()
{
    transmittedSerialNumber_ = htonl(sensorSerialNumber_);
    
    Serialization::serializeUInt32(transmittedSerialNumber_, transmittedSerialNumberData_, true);

    transmittedMessage_.insert(transmittedMessage_.end(), transmittedSerialNumberData_, transmittedSerialNumberData_ + 4);
    transmittedMessage_.push_back(transmittedCommandID_);
    transmittedMessage_.insert( transmittedMessage_.end(), transmittedData_.begin(), transmittedData_.end());

    transmittedCRC8_ = crc8_.crcFast((uint8_t *)transmittedMessage_.data(), transmittedMessage_.size());
    transmittedMessage_.push_back(transmittedCRC8_);
    transmittedNumOfBytes_ = transmittedMessage_.size(); // Plus one because of CRC8 byte, four for number of transmitted bytes

    return transmittedNumOfBytes_;
}

bool UDPTransmitter::sendMessage()
{
    bool success;
    int errno;

    errno = transmitMessage(transmittedMessage_.data(), transmittedNumOfBytes_);
    

    if (debugModeStreaming_)
    {
        std::cout << "From UDP Manager, Transmitted serialNumber is := " << std::hex << +transmittedSerialNumberData_ << std::dec << std::endl;
        std::cout << "From UDP Manager, Transmitted command number is := " << std::hex << +transmittedCommandID_ << std::dec << std::endl;
        std::cout << "From UDP Manager, Transmitted command data is := ";
        for (unsigned int i = 0; i < transmittedData_.size(); ++i)
            std::cout << std::hex << transmittedData_[i];
        std::cout << std::dec << std::endl;

        std::cout << "From UDP Manager, transmitted msg: ";
        for (unsigned int i = 0; i < transmittedMessage_.size(); ++i)
            std::cout << std::hex << +transmittedMessage_[i] << std::endl;
        std::cout << std::dec << std::endl;

        std::cout << "From UDP Manager, transmitted number of bytes is := " << transmittedNumOfBytes_ << std::endl;
    }
    transmittedMessage_.clear();
    transmittedData_.clear();

    success = errno == 0;

    return success;
}


void UDPTransmitter::sendMessages(std::vector<Command> &commands)
{
    unsigned int currentSentMessagesTotalSize = 0;
    unsigned int counter = 0;
    for (int i = 0; i < commands.size(); ++i)
    {

        transmittedCommandID_   = commands[i].commandID_;
        transmittedData_        = commands[i].command_;

        uint32_t numBytes = formMessage();

        currentSentMessagesTotalSize += numBytes;

        if(currentSentMessagesTotalSize > 32001)
        {
            // std::cout << "lowering sendMessage rate due to message sizes!"<< std::endl;
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
            currentSentMessagesTotalSize = numBytes;
        }

        sendMessage();

        counter++;
    }
}