/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "TCPClient.h"

TCPClient::TCPClient(struct in_addr address, unsigned int remoteReceivePort)
{
#ifdef DEBUG
    debugMode_ = true;
#endif
    open_ = false;

    remoteReceivePort_ = remoteReceivePort;
    remoteAddress_.sin_addr.s_addr = address.s_addr;

    setConnected(false);
    crc8_.crcInit();
    tcpSettings_ = static_cast<int>(TCPMessageTypeSettings::BOTH);
    openSocket();
}

bool TCPClient::openSocket()
{
#ifdef __linux__ 
    socketEndpoint_ = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, IPPROTO_TCP);
#elif _WIN32
    WSADATA wsaData;
    int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (iResult != 0) {
        std::cout << "WSAStartup failed: " << iResult << std::endl;
        return 1;
    }
    socketEndpoint_ = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    unsigned long argp = 1;// 1 to set non-blocking, 0 to set re-usable
    int result = setsockopt(socketEndpoint_, SOL_SOCKET, SO_REUSEADDR, (char*)&argp, sizeof(argp));
    if (result != 0)
    {
        std::cout << "setsockopt() error " << result << std::endl;
        return EXIT_FAILURE;
    }

    argp = 1;// 1 to set non-blocking, 0 to set blocking
    if (ioctlsocket(socketEndpoint_, FIONBIO, &argp) == SOCKET_ERROR)
    {
        std::cout << "ioctlsocket() error " << WSAGetLastError() << std::endl;
        return EXIT_FAILURE;
    }
#endif
    
    if (socketEndpoint_ < 0)
    {
        std::cout << "Error while opening transmitting TCP socket" << std::endl;
        open_ = false;
        return false;
    }

    /*Set the IP addresses for TCP message*/
    remoteAddress_.sin_family = AF_INET;
    remoteAddress_.sin_port = htons(remoteReceivePort_);

    memset(remoteAddress_.sin_zero, '\0', sizeof(remoteAddress_.sin_zero));

    open_ = true;
    if (debugMode_)
    {
        std::cout << "TCP socket opened" << std::endl;
    }
    return true;
}

void TCPClient::connectToServer()
{
    if(connected_)
    {
        std::cout << "already connected" << std::endl;
        return;
    }

    if (!open_ && !openSocket())
    {
        std::cout << "socket not open" << std::endl;
        return;
    }
    
    //remoteAddress_.sin_addr.s_addr = serverAddress.s_addr;

    if (debugMode_)
    {
        std::cout << "From TCP Transmitter, setting server IP address to := " << inet_ntoa(remoteAddress_.sin_addr) << std::endl;
    }
    int connection = connect(socketEndpoint_, (struct sockaddr *)&remoteAddress_, sizeof(remoteAddress_));

    if (connection < 0)
    {

#ifdef __linux__ 
        if (errno != EINPROGRESS)
        {
            perror(" Error connecting TCP port, error is := ");
            setConnected(false);
            return;
        }
#elif _WIN32
        if (WSAGetLastError() != WSAEWOULDBLOCK)
        {
            perror(" Error connecting TCP port, error is := ");
            setConnected(false);
            return;
        }
#endif

        fd_set writeFD, exceptFD;

        FD_ZERO(&writeFD);
        FD_SET(socketEndpoint_, &writeFD);

        FD_ZERO(&exceptFD);
        FD_SET(socketEndpoint_, &exceptFD);

        timeval tv;
        tv.tv_sec = 1;
        tv.tv_usec = 0;

        int result = select(socketEndpoint_ + 1, NULL, &writeFD, &exceptFD, &tv);
        if (result == -1 || result == 0)
        {
            perror(" Error on select on TCP port, error is := ");
            setConnected(false);
            return;
        }
        int connV = connect(socketEndpoint_, (struct sockaddr*) & remoteAddress_, sizeof(remoteAddress_));
        if (debugMode_)
        {
            std::cout << "value of connV: " << connV << std::endl;
        }
        setConnected(false);
        if (connV >= 0)
        {
            setConnected(true);
        }
#ifdef _WIN32
        if (WSAGetLastError() == WSAEISCONN)
        {
            setConnected(true);
        }
#endif

        if(!connected_)
        {
            perror("STILL NO CONNECTION POSSIBLE.." );
            return;
        }
    }
    else
    {
        perror("SUCCESSFUL CONNECTION");
        setConnected(true);
    }
}

int TCPClient::transmitMessage(uint8_t * transmittedMessage, unsigned int transmittedNumOfBytes)
{
    int errsv = 0;
    
    if (!open_)
    {
        errsv = - 1;
        return errsv;
    }

    if(connected_ && open_)
    {
        if (transmittedNumOfBytes > bufferSize_)
        {
            if (debugMode_)
                std::cout << "Number of bytes is larger than maximum message size, number of bytes := " << transmittedNumOfBytes << std::endl;
            return false;
        }
#ifdef __linux__
        if (sendto(socketEndpoint_, &transmittedMessage[0], transmittedNumOfBytes, MSG_NOSIGNAL, (struct sockaddr*) & remoteAddress_, sizeof(remoteAddress_)) == -1)
#elif _WIN32
        if (sendto(socketEndpoint_, reinterpret_cast<char*>(&transmittedMessage[0]), transmittedNumOfBytes, 0, (struct sockaddr*) & remoteAddress_, sizeof(remoteAddress_)) == -1)
#endif
        {
            errsv = errno;
            if (errsv == 32)
            {
                setConnected(false);
                closeSocket();
            }
            if(debugMode_)
            {
                perror(" Error sending message to port, error is := ");
            }
            return false;
        }
        return true;
    }

    return errsv;
}

bool TCPClient::receiveMessage()
{
    if (!open_)
    {
        return false;
    }

    if (connected_)
    {

#ifdef __linux__ 
        receivedNumOfBytes_ = read(socketEndpoint_, receivedMessage_, sizeof(receivedMessage_));
        if (receivedNumOfBytes_ <= 0)
        {
            int errsv = errno;
            if (debugMode_ && (errsv != 11))
                perror("Error while receiving messages in TCPReceiver, error is");
            return false;
        }
#elif _WIN32
        receivedNumOfBytes_ = recv(socketEndpoint_, receivedMessage_, sizeof(receivedMessage_), 0);
        if (receivedNumOfBytes_ <= 0)
        {
            if (debugMode_ && (WSAGetLastError() != WSAEWOULDBLOCK))
                perror("Error while receiving messages in TCPReceiver, error is");
            return false;
        }
#endif

        else
        {
            if (receivedNumOfBytes_ > bufferSize_)
            {
                if (debugMode_)
                    std::cout << "Received TCP Message is too big, received num of bytes is := " << receivedNumOfBytes_ << std::endl;
                return false;
            }

            if (debugMode_)
                std::cout << "received msg: " << receivedMessage_ << "received num of bytes := " << receivedNumOfBytes_ << std::endl;
            return true;
        }
    }
    else
    {
        return false;
    }
    
}

int TCPClient::transmitMessage()
{
    int errornumber = 0;

    if (transmittedMessageType_ == MessageTypes::ACKNOWLEDGEMENT)
    {
        return transmitMessage(transmittedMessage_.data(), transmittedNumOfBytes_);
    }

    if (tcpSettings_ == static_cast<int>(TCPMessageTypeSettings::INACTIVE))
    {
        return 0;
    }

    if (transmittedMessageType_ == MessageTypes::STREAMING)
    {
        if (tcpSettings_ == static_cast<int>(TCPMessageTypeSettings::BOTH) || tcpSettings_ == static_cast<int>(TCPMessageTypeSettings::STREAMING))
        {
            errornumber = transmitMessage(transmittedMessage_.data(), transmittedNumOfBytes_);
        }
    }
    else if (transmittedMessageType_ == MessageTypes::INTERMITTENT || transmittedMessageType_ == MessageTypes::COMMAND)
    {
        if (tcpSettings_ == static_cast<int>(TCPMessageTypeSettings::BOTH) || tcpSettings_ == static_cast<int>(TCPMessageTypeSettings::INTERMITTENT))
        {
            errornumber = transmitMessage(transmittedMessage_.data(), transmittedNumOfBytes_);
        }
    }

    if (errornumber == SocketErrors::BROKEN_PIPE)
    {
        tcpSettings_ = static_cast<int>(TCPMessageTypeSettings::INACTIVE);
        remoteAddress_.sin_addr.s_addr = 0;
    }

    return errornumber;
}

uint32_t TCPClient::formMessage()
{
    transmittedSerialNumber_ = htonl(sensorSerialNumber_);
    
    Serialization::serializeUInt32(transmittedSerialNumber_, transmittedSerialNumberData_, true);

    transmittedMessage_.insert(transmittedMessage_.end(), transmittedSerialNumberData_, transmittedSerialNumberData_ + 4);
    transmittedMessage_.push_back(transmittedCommandID_);
    transmittedMessage_.insert( transmittedMessage_.end(), transmittedData_.begin(), transmittedData_.end());

    transmittedCRC8_ = crc8_.crcFast((uint8_t *)transmittedMessage_.data(), transmittedMessage_.size());
    transmittedMessage_.push_back(transmittedCRC8_);
    transmittedNumOfBytes_ = transmittedMessage_.size(); // Plus one because of CRC8 byte, four for number of transmitted bytes

    return transmittedNumOfBytes_;
}

bool TCPClient::sendMessage()
{
    bool success;
    int errornumber = 0;

    auto iter = commandValues.find(transmittedCommandID_);
    if (iter != commandValues.end())
    {
        transmittedMessageType_ = std::get<2>(iter->second);
    }

    errornumber = transmitMessage();

    // if (debugModeStreaming_)
    // {
    //     std::cout << "From UDP Manager, Transmitted serialNumber is := " << std::hex << +transmittedSerialNumberData_ << std::dec << std::endl;
    //     std::cout << "From UDP Manager, Transmitted command number is := " << std::hex << +transmittedCommandID_ << std::dec << std::endl;
    //     std::cout << "From UDP Manager, Transmitted command data is := ";
    //     for (unsigned int i = 0; i < transmittedData_.size(); ++i)
    //         std::cout << std::hex << transmittedData_[i];
    //     std::cout << std::dec << std::endl;

    //     std::cout << "From UDP Manager, transmitted msg: ";
    //     for (unsigned int i = 0; i < transmittedMessage_.size(); ++i)
    //         std::cout << std::hex << +transmittedMessage_[i] << std::endl;
    //     std::cout << std::dec << std::endl;

    //     std::cout << "From UDP Manager, transmitted number of bytes is := " << transmittedNumOfBytes_ << std::endl;
    // }
    transmittedMessage_.clear();
    transmittedData_.clear();

    success = errornumber == 0;

    return success;
}

void TCPClient::sendMessages(std::vector<Command> &commands)
{
    unsigned int currentSentMessagesTotalSize = 0;
    unsigned int counter = 0;
    for (int i = 0; i < commands.size(); ++i)
    {

        transmittedCommandID_   = commands[i].commandID_;
        transmittedData_        = commands[i].command_;

        uint32_t numBytes = formMessage();

        currentSentMessagesTotalSize += numBytes;

        if(currentSentMessagesTotalSize > 32001)
        {
            // std::cout << "lowering sendMessage rate due to message sizes!"<< std::endl;
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
            currentSentMessagesTotalSize = numBytes;
        }
        
        sendMessage();

        counter++;
    }
}

void TCPClient::closeSocket()
{
    open_ = false;
#ifdef __linux__ 
    close(socketEndpoint_);
#elif _WIN32
    closesocket(socketEndpoint_);
#endif
}

TCPClient::~TCPClient()
{
#ifdef __linux__ 
    close(socketEndpoint_);
#elif _WIN32
    closesocket(socketEndpoint_);
#endif
}
