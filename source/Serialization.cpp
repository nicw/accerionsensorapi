/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#include "Serialization.h"

/*Serializes a 16-bit integer into big-endian byte array*/
void Serialization::serializeUInt16(uint16_t in, uint8_t* out, bool bigEndian)
{
    /*Big-endian*/
    if(bigEndian)
    {
        out[0] = (in & 0xFF);
        out[1] = ((in >> 8)  & 0xFF);
    }
    /*Little-endian*/
    else
    {
        out[0] = ((in >> 8 ) & 0xFF);
        out[1] = (in & 0xFF);
    }
}

/*Serializes a 32-bit integer into big-endian byte array*/
void Serialization::serializeUInt32(uint32_t in, uint8_t* out, bool bigEndian)
{
    /*Big-endian*/
    if(bigEndian)
    {
        out[0] = (in & 0xFF);
        out[1] = ((in >> 8)  & 0xFF);
        out[2] = ((in >> 16) & 0xFF);
        out[3] = ((in >> 24) & 0xFF);
    }
    /*Little-endian*/
    else
    {
        out[0] = ((in >> 24) & 0xFF);
        out[1] = ((in >> 16) & 0xFF);
        out[2] = ((in >> 8 ) & 0xFF);
        out[3] = (in & 0xFF);
    }
}

/*Serializes a 64-bit integer into big-endian byte array*/
void Serialization::serializeUInt64(uint64_t in, uint8_t* out, bool bigEndian)
{
    /*Big-endian*/
    if(bigEndian)
    {
        out[0] = (in & 0xFF);
        out[1] = ((in >> 8 ) & 0xFF);
        out[2] = ((in >> 16) & 0xFF);
        out[3] = ((in >> 24) & 0xFF);
        out[4] = ((in >> 32) & 0xFF);
        out[5] = ((in >> 40) & 0xFF);
        out[6] = ((in >> 48) & 0xFF);
        out[7] = ((in >> 56) & 0xFF);
    }
    /*Little-endian*/
    else
    {
        out[0] = ((in >> 56) & 0xFF);
        out[1] = ((in >> 48) & 0xFF);
        out[2] = ((in >> 40) & 0xFF);
        out[3] = ((in >> 32) & 0xFF);
        out[4] = ((in >> 24) & 0xFF);
        out[5] = ((in >> 16) & 0xFF);
        out[6] = ((in >> 8 ) & 0xFF);
        out[7] = (in & 0xFF);
    }
}

/**
 * \brief Method to convert from network to host order
 * \param input uint64_t the 64 bit value that has to be converted
 * \return uint64_t in host order
 **/
uint64_t Serialization::ntoh64(const uint64_t *input)
{
    uint64_t rval;
    uint8_t *data = (uint8_t *)&rval;

    data[0] = *input >> 56;
    data[1] = *input >> 48;
    data[2] = *input >> 40;
    data[3] = *input >> 32;
    data[4] = *input >> 24;
    data[5] = *input >> 16;
    data[6] = *input >> 8;
    data[7] = *input >> 0;

    return rval;
}

/**
 * \brief Method to convert from host to network order
 * \param input uint64_t the 64 bit value that has to be converted
 * \return uint64_t in network order
 **/
uint64_t Serialization::hton64(const uint64_t *input)
{
    return (ntoh64(input));
}