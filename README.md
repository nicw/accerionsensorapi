# AccerionSensorAPI

C++ Wrapper/API for communicating with Sensors.

The API was designed to make integration as simple as possible. With only minimal lines of code you can already start recieving messages from a sensor. Below is shown how.

First you need to compile the API. To do this

1. mkdir build
2. cd build
3. cmake ..
4. make AccerionSensorAPI -j$(nproc)
5. sudo make install(optional)

**If you did step 5** you can use the following in your CMakeList.txt(if you use CMake that is..):
```
find_package(AccerionSensorAPI)
if(AccerionSensorAPI_FOUND)
    target_link_libraries(yourApplication  AccerionSensorAPI)
else()
    message(STATUS "AccerionSensorAPI not found, make sure to get it first..")
endif(AccerionSensorAPI_FOUND)
```

**If you did step 5** you can start using the API in your code by including it like this:

`#include <AccerionSensorAPI/AccerionSensorAPI.h>`

**If you chose not to install it**, make sure to properly link to the compiled library file and include the libraries of the API in your project.
Then your CMakeLists file could look like this:

```
set(LIBRARYPATH      "/home/test/AccerionSensorAPI/build/libAccerionSensorAPI.a")
set(INCLUDEPATH      "/home/test/AccerionSensorAPI/include")

find_library(ACCAPI ${LIBRARYPATH})
include_directories(${INCLUDEPATH})
target_link_libraries(yourApplication ${LIBRARYPATH})
```
**If you chose not to install it** you would include the file like this:

`#include "AccerionSensorAPI.h"`

First get an instance of the SensorManager:

`AccerionSensorManager* sensorManager = AccerionSensorManager::getInstance();`


This manager will listen to any messages in the subnet and identify sensors. So you can for instance do the following:

`std::list<std::pair<Address, std::string>> listOfSensors = sensorManager->getAllSensors();`

Note that the API builds this list based on sensors it detects. So in order for the sensor to show up in this list, 
the AccerionSensorManager has to be up and running for a few seconds while the sensor is active.

If you find the sensor you desire in that list, you can do this to get an AccerionSensor object:

`AccerionSensor* mySensor = sensorManager->getAccerionSensorByIP(remoteIP, localIP, ConnectionType::CONNECTION_TCP);`


Here remoteIP and localIP are structures of the type Address(these are defined in structs.h and are simply 4 uint8_t's to store the IP). 
The last parameter is used to configure the preferred connection method, in the above snippet we chose to have all messages over TCP, therefore disabled UDP. 


Now that you have an AccerionSensor object you can start interfacing with the sensor itself. If we want to listen to any position output for instance, we can do the following:
* First make sure you have a method that has the following signature: void methodname(CorrectedPose cp);
* Then you can register this method as a callback method, so that it gets invoked on every correctedpose message that comes in:

`auto callback = std::bind(&yourclass::methodname, this, std::placeholders::_1);`

`mySensor->subscribeToCorrectedPose(callback);`

Now that method gets invoked on each corrected pose message. The CorrectedPose parameter contains data like x, y, heading, velocity etc.

 
